$mailserver = "1.2.3.4"
$from = "docker@docker.local"
$to = "me@me.me"
$subject = "New Docker-EE Version"
$pathToStoreLatesVersion = "C:\latestDockerEEVersion.txt"

try {
    $lastKnownVersion = Get-Content $pathToStoreLatesVersion -ErrorAction SilentlyContinue

    $response = Invoke-RestMethod -Uri "https://go.microsoft.com/fwlink/?LinkID=825636" 

    $newestVersion = $response.versions.PSObject.Properties.Name | sort -desc | select -First 1

    if($newestVersion -ne $lastKnownVersion) {
        Write-Host "New version $newestVersion found (old version: $lastKnownVersion)"
        Send-MailMessage -SmtpServer $mailserver -From $from -Subject $subject -To $to -Body "$newestVersion `n`n Script runs on $($env:COMPUTERNAME)"
        $newestVersion | Out-File $pathToStoreLatesVersion
    }

}
catch {
    $_.Exception
    Send-MailMessage -SmtpServer $mailserver -From $from -Subject $subject -To $to -Body "Error: $($_.Exception) `n`n Script runs on $($env:COMPUTERNAME)"
}
